﻿namespace YouCan.DataAccess.Core
{
    public enum CommandType
    {
        UserCommand,
        KeepAliveCommand
    }
}
