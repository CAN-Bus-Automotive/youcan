﻿namespace YouCan.DataAccess.Core
{
    public enum EngineType
    {
        Gasoline,
        Petrol,
        Diesel,
        Hybrid,
        Electric
    }
}
