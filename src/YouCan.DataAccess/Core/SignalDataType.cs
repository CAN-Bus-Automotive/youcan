﻿namespace YouCan.DataAccess.Core
{
    public enum SignalDataType
    {
        Uint,
        Bool,
        Int
    }
}
