﻿using YouCan.DataAccess.Core;

namespace YouCan.DataAccess.Models
{
    public class DbcCommand
    {
        public CommandType CommandType { get; set; } = CommandType.UserCommand;

        public string CommandName { get; set; }
    }
}
