﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace YouCan.DataAccess.Models
{
    public class DbcFile
    {
        public string FileVersion { get; set; }

        public Vehicle Vehicle { get; set; }

        public List<DbcMessage> Messages { get; set; } = new List<DbcMessage>();

        public ObservableCollection<DbcCommand> Commands { get; set; } = new ObservableCollection<DbcCommand>();
    }
}
