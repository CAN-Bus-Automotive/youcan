﻿using System.Collections.Generic;

namespace YouCan.DataAccess.Models
{
    public class DbcMessage
    {
        public List<DbcSignal> Signals { get; set; } = new List<DbcSignal>();
    }
}
