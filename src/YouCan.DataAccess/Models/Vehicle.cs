﻿using YouCan.DataAccess.Core;

namespace YouCan.DataAccess.Models
{
    public class Vehicle
    {
        public string Brand { get; set; }

        public string Model { get; set; }

        public int ModelYear { get; set; }

        public double EngineVolume { get; set; }

        public EngineType EngineType { get; set; }
    }
}
