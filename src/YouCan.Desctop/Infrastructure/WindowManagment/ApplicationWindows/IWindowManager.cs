﻿using System.Threading.Tasks;

namespace YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows
{
    public interface IWindowManager
    {
        Task<string> ShowFolderDialog();

        Task<string[]> ShowOpenFileDialog();

        Task<string> ShowSaveFileDialog();
    }
}