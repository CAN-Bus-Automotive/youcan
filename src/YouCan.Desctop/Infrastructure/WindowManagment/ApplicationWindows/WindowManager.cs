﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Avalonia.Controls;

namespace YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows
{
    [ExcludeFromCodeCoverage]
    public class WindowManager : WindowManagerBase, IWindowManager
    {
        public Task<string[]> ShowOpenFileDialog()
        {
            var fileDialog = new OpenFileDialog();
            var fileDialogResult = fileDialog.ShowAsync(GetMainWindow());

            return fileDialogResult;
        }

        public Task<string> ShowSaveFileDialog()
        {
            var fileDialog = new SaveFileDialog();
            var fileDialogResult = fileDialog.ShowAsync(GetMainWindow());

            return fileDialogResult;
        }

        public Task<string> ShowFolderDialog()
        {
            var folderDialog = new OpenFolderDialog();
            var folderDialogResult = folderDialog.ShowAsync(GetMainWindow());

            return folderDialogResult;
        }
    }
}
