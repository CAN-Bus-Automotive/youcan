﻿using System;
using Avalonia;
using Avalonia.Controls;

namespace YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows
{
    public class WindowManagerBase
    {
        /// <summary>
        /// Return Main Windows of application.
        /// </summary>
        /// <returns>Window <see cref="Window"/>.</returns>
        /// <exception cref="Exception">If window not found.</exception>
        protected Window GetMainWindow()
        {
            // TODO add implementation
            return Application.Current.MainWindow;
        }
    }
}
