﻿using System.Threading.Tasks;

namespace YouCan.Desctop.Infrastructure.WindowManagment.MessageBox
{
    public interface IMessageBoxManager
    {
        Task<string> ShowInfo(string title, string message);

        Task<string> ShowError(string title, string message);
    }
}