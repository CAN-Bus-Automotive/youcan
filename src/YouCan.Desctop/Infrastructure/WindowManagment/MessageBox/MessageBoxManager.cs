﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Avalonia;
using MessageBox.Avalonia.DTO;
using MessageBox.Avalonia.Enums;
using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;
using MsgBox = MessageBox.Avalonia;

namespace YouCan.Desctop.Infrastructure.WindowManagment.MessageBox
{
    [ExcludeFromCodeCoverage]
    public class MessageBoxManager : WindowManagerBase, IMessageBoxManager
    {
        public async Task<string> ShowInfo(string title, string message)
        {
            var msgbox = new MsgBox.MessageBoxWindow(new MessageBoxParams
            {
                Button = ButtonEnum.Ok,
                ContentTitle = title,
                ContentMessage = message,
                Icon = Icon.Info,
                Style = Style.None
            });
            var result = await msgbox.ShowDialog(Application.Current.MainWindow);

            return result;
        }

        public async Task<string> ShowError(string title, string message)
        {
            var msgbox = new MsgBox.MessageBoxWindow(new MessageBoxParams
            {
                Button = ButtonEnum.Ok,
                ContentTitle = title,
                ContentMessage = message,
                Icon = Icon.Error,
                Style = Style.None
            });
            var result = await msgbox.ShowDialog(Application.Current.MainWindow);

            return result;
        }
    }
}
