﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Avalonia;
using Avalonia.Logging.Serilog;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using YouCan.Desctop.Views;

namespace YouCan.Desctop
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        public static void Main(string[] args) => BuildAvaloniaApp().Start(AppMain, args);

        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .UseReactiveUI()
                .LogToDebug();

        private static void AppMain(Application app, string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("YOUCAN_ENVIRONMENT")?.ToLower();
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{envName}.json", true, true)
                .AddEnvironmentVariables()
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            ServiceProvider sp = ViewModelLocator.Initialize();

            var logger = sp.GetService<ILogger<Program>>();
            var mainWindow = sp.GetService<MainWindow>();

            try
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                logger.LogInformation("Starting a desktop application");
                app.Run(mainWindow);
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "The application terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("Unhandled exception");
        }
    }
}
