﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;
using YouCan.Desctop.Infrastructure.WindowManagment.MessageBox;
using YouCan.Desctop.ViewModels;
using YouCan.Desctop.Views;
using YouCan.Services.Dbc;
using YouCan.Services.WireShark;
using YouCan.Services.WireShark.Mappers;
using YouCan.Services.WireShark.Mappers.Core;

namespace YouCan.Desctop
{
    public class ViewModelLocator
    {
        private static ServiceProvider _serviceProvider;

        public static bool IsInitialized { get; }

        public MainWindowViewModel MainWindowViewModel => _serviceProvider.GetService<MainWindowViewModel>();

        public DbcViewModel DbcViewModel => _serviceProvider.GetService<DbcViewModel>();

        public ConnectionViewModel ConnectionViewModel => _serviceProvider.GetService<ConnectionViewModel>();

        public StatusBarViewModel StatusBarViewModel => _serviceProvider.GetService<StatusBarViewModel>();

        public static ServiceProvider Initialize()
        {
            if (!IsInitialized)
            {
#if DESIGNMODE
                // Some actions in DESIGNMODE
#endif
                var services = new ServiceCollection();

                services.AddLogging(logBuilder => logBuilder.AddSerilog());

                // Register services
                services.AddSingleton<IDbcService, DbcService>();
                services.AddSingleton<IWindowManager, WindowManager>();
                services.AddSingleton<IMessageBoxManager, MessageBoxManager>();
                services.AddTransient<WireSharkService<string>>();
                services.AddTransient<IMessageMapper<string>, LawicelMessageMapper>();

                // Register windows
                services.AddSingleton<MainWindow>();

                // Register view models
                services.AddSingleton<MainWindowViewModel>();
                services.AddSingleton<DbcViewModel>();
                services.AddSingleton<ConnectionViewModel>();
                services.AddSingleton<StatusBarViewModel>();

                _serviceProvider = services.BuildServiceProvider();
            }

            return _serviceProvider;
        }
    }
}
