﻿using System.Reactive;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using ReactiveUI;
using YouCan.DataAccess.Models;
using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;
using YouCan.Desctop.ViewModels.Core;
using YouCan.Services.Dbc;

namespace YouCan.Desctop.ViewModels
{
    public class DbcViewModel : ViewModelBase
    {
        private readonly IDbcService _dbcService;
        private readonly IWindowManager _windowManager;
        private readonly ILogger<DbcViewModel> _logger;

        private readonly ReactiveCommand<Unit, Unit> _openDbcFileCmd;

        public DbcViewModel(ILogger<DbcViewModel> logger, IDbcService dbcService, IWindowManager windowManager)
        {
            _dbcService = dbcService;
            _windowManager = windowManager;
            _logger = logger;

            _openDbcFileCmd = ReactiveCommand.CreateFromTask(async () =>
           {
               var filePath = await _windowManager.ShowOpenFileDialog();
               if (filePath.Length == 0)
               {
                   return;
               }
               DbcFile = _dbcService.ReadDbcFile(filePath[0]);
               CurrentDbcFileName = filePath[0];
           });
        }

        public string CurrentDbcFileName { get; set; }

        public DbcMessage SelectedMessage { get; set; }

        public DbcCommand SelectedCommand { get; set; }

        public DbcFile DbcFile { get; set; }

        public ICommand OpenDbcFileCmd => _openDbcFileCmd;
    }
}
