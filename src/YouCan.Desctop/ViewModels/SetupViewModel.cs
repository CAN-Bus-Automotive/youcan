﻿using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;
using YouCan.Desctop.ViewModels.Core;

namespace YouCan.Desctop.ViewModels
{
    public class SetupViewModel : ViewModelBase
    {
        private readonly IWindowManager _windowManager;

        public SetupViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
        }
    }
}
