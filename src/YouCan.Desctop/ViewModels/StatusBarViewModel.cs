﻿using System;
using System.Reactive.Linq;
using YouCan.Desctop.ViewModels.Core;

namespace YouCan.Desctop.ViewModels
{
    public class StatusBarViewModel : ViewModelBase
    {
        public IObservable<string> Now =>
            Observable.Timer(DateTimeOffset.Now, TimeSpan.FromSeconds(1))
            .Select(_ => DateTime.Now.ToString());
    }
}
