﻿using Avalonia.Markup.Xaml;
using YouCan.Desctop.Views.Core;

namespace YouCan.Desctop.Views.Components.DbcView
{
    public class CommandItemComponent : BaseControl
    {
        public CommandItemComponent()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
