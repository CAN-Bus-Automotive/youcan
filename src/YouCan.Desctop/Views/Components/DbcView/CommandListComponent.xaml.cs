﻿using Avalonia.Markup.Xaml;
using YouCan.Desctop.Views.Core;

namespace YouCan.Desctop.Views.Components.DbcView
{
    public class CommandListComponent : BaseControl
    {
        public CommandListComponent()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
