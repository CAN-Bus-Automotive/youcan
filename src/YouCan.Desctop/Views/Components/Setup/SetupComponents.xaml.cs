﻿using Avalonia.Markup.Xaml;
using YouCan.Desctop.Views.Core;

namespace YouCan.Desctop.Views.Components.Setup
{
    public class SetupComponents : BaseControl
    {
        public SetupComponents()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
