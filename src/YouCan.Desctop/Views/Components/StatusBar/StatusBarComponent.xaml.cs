﻿using Avalonia.Markup.Xaml;
using YouCan.Desctop.Views.Core;

namespace YouCan.Desctop.Views.Components.StatusBar
{
    public class StatusBarComponent : BaseControl
    {
        public StatusBarComponent()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
