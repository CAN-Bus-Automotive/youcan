﻿using System;
using Avalonia.Controls;
using PropertyChanged;

namespace YouCan.Desctop.Views.Core
{
    /// <summary>
    /// Workaround for model activation.
    /// </summary>
    [DoNotNotifyAttribute]
    public class BaseControl : UserControl
    {
        public BaseControl(bool activate = true)
        {
            ApplyDataContext();
        }

        private void ApplyDataContext()
        {
            object obj;
            Avalonia.Application.Current.Resources.TryGetResource("Locator", out obj);

            ViewModelLocator vmlocator = obj as ViewModelLocator;

            if (vmlocator != null)
            {
                DataContext = vmlocator;
            }
            else
            {
                throw new Exception("View model locator doesn't found");
            }
        }
    }
}
