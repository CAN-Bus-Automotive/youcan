﻿using Avalonia;
using Avalonia.Markup.Xaml;
using YouCan.Desctop.Views.Core;

namespace YouCan.Desctop.Views
{
    public class MainWindow : BaseWindow
    {
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
