﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using YouCan.DataAccess.Models;

namespace YouCan.Services.Dbc
{
    public class DbcService : IDbcService
    {
        private readonly ILogger<DbcService> _logger;

        public DbcService(ILogger<DbcService> logger)
        {
            _logger = logger;
        }

        public DbcFile ReadDbcFile(string filePath)
        {
            try
            {
                // Deserialize JSON directly from a file
                using (StreamReader file = File.OpenText(filePath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    var dbcFile = (DbcFile)serializer.Deserialize(file, typeof(DbcFile));

                    return dbcFile;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw new DbcServiceException("DBC file reading error", e.InnerException);
            }
        }
    }
}
