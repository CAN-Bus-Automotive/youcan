﻿using System;

namespace YouCan.Services.Dbc
{
    public class DbcServiceException : Exception
    {
        public DbcServiceException()
            : base()
        {
        }

        public DbcServiceException(string message)
            : base(message)
        {
        }

        public DbcServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
