﻿using System.Threading.Tasks;
using YouCan.DataAccess.Models;

namespace YouCan.Services.Dbc
{
    public interface IDbcService
    {
        DbcFile ReadDbcFile(string filePath);
    }
}