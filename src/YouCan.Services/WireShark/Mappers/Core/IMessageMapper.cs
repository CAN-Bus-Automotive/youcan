﻿namespace YouCan.Services.WireShark.Mappers.Core
{
    public interface IMessageMapper<S>
    {
        byte[] Map(S message);
    }
}
