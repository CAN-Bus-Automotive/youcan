﻿using System.Diagnostics;
using Wireshark;
using YouCan.Services.WireShark.Mappers.Core;

namespace YouCan.Services.WireShark
{
    public class WireSharkService<S>
    {
        private readonly IMessageMapper<S> _messageMapper;
        private WiresharkSender _ws;
        private string _pipeName;

        public WireSharkService(IMessageMapper<S> messageMapper)
        {
            _messageMapper = messageMapper;
        }

        public void StartPipe(string name)
        {
            _pipeName = name;
            _ws?.Dispose();
            _ws = new WiresharkSender(_pipeName, 227);
        }

        public void ProduceToPipe(S message)
        {
            var msg = _messageMapper.Map(message);
            if (_ws.isConnected)
            {
                byte[] arr = new byte[] { 01, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0xEE, 0xEE, 3, 0, 0, 4 };
                _ws.SendToWireshark(msg, 0, msg.Length);
            }
        }

        public void StartWiresharkGtk(string path)
        {
            var process = new Process();
            process.StartInfo.FileName = path + @"/wireshark-gtk.exe";
            process.StartInfo.Arguments = $@"-ni \\.\pipe\{_pipeName} -k";
            process.Start();
        }

        public void StartWireshark(string path)
        {
            var process = new Process();
            process.StartInfo.FileName = path + @"/wireshark.exe";
            process.StartInfo.Arguments = $@"-ni \\.\pipe\{_pipeName} -k";
            process.Start();
        }
    }
}
