﻿using Xunit;

namespace YouCan.Tests.IntegrationTests.Core
{
    [CollectionDefinition(nameof(IntegrationCollection))]
    public class IntegrationCollection : ICollectionFixture<IntegrationFixture>
    {
    }
}
