﻿using Microsoft.Extensions.DependencyInjection;
using System;
using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;
using YouCan.Desctop.Infrastructure.WindowManagment.MessageBox;
using YouCan.Desctop.ViewModels;
using YouCan.Desctop.Views;
using YouCan.Services.Dbc;
using YouCan.Tests.IntegrationTests.Fakes;

namespace YouCan.Tests.IntegrationTests.Core
{
    public class IntegrationFixture : IDisposable
    {
        private static ServiceProvider _serviceProvider;

        public IntegrationFixture()
        {
            var services = new ServiceCollection();

            services.AddLogging();

            // Register services
            services.AddSingleton<IDbcService, DbcServiceFake>();
            services.AddSingleton<IWindowManager, WindowManagerFake>();
            services.AddSingleton<IMessageBoxManager, MessageBoxManagerFake>();

            // Register view models
            services.AddSingleton<MainWindowViewModel>();
            services.AddSingleton<DbcViewModel>();

            _serviceProvider = services.BuildServiceProvider();
        }

        public IDbcService DbcService => _serviceProvider.GetService<IDbcService>();

        public IWindowManager WindowManager => _serviceProvider.GetService<IWindowManager>();

        public IMessageBoxManager MessageBoxManager => _serviceProvider.GetService<IMessageBoxManager>();

        public DbcViewModel DbcViewModel => _serviceProvider.GetService<DbcViewModel>();

        public MainWindowViewModel MainWindowViewModel => _serviceProvider.GetService<MainWindowViewModel>();

        public void Dispose()
        {
            _serviceProvider.Dispose();
        }
    }
}
