﻿using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;
using YouCan.Desctop.Infrastructure.WindowManagment.MessageBox;
using YouCan.Desctop.ViewModels;
using YouCan.Services.Dbc;

namespace YouCan.Tests.IntegrationTests.Core
{
    public class TestBase
    {
        public TestBase(IntegrationFixture integrationFixture)
        {
            DbcService = integrationFixture.DbcService;
            WindowManager = integrationFixture.WindowManager;
            MessageBoxManager = integrationFixture.MessageBoxManager;
            DbcViewModel = integrationFixture.DbcViewModel;
            MainWindowViewModel = integrationFixture.MainWindowViewModel;
        }

        public IDbcService DbcService { get; }

        public IWindowManager WindowManager { get; }

        public IMessageBoxManager MessageBoxManager { get; }

        public DbcViewModel DbcViewModel { get; }

        public MainWindowViewModel MainWindowViewModel { get; }
    }
}
