﻿using FluentAssertions;
using System.Threading.Tasks;
using Xunit;
using YouCan.Desctop.ViewModels;
using YouCan.Tests.IntegrationTests.Core;
using YouCan.Tests.IntegrationTests.Fakes;

namespace YouCan.Tests.IntegrationTests
{
    [Collection(nameof(IntegrationCollection))]
    public class DbcViewModelTests : TestBase
    {
        public DbcViewModelTests(IntegrationFixture integrationFixture) :
            base(integrationFixture)
        {
        }

        [Fact]
        public void ReadDbcFileCmd_ShuldReturnDbcSignal()
        {
            // Act
            DbcViewModel.OpenDbcFileCmd.Execute(null);
            // Assert
            Assert.Equal("TestPathAndFileName", DbcViewModel.CurrentDbcFileName);
            DbcViewModel.DbcFile.Should().BeEquivalentTo(new DbcFileFake());
        }
    }
}
