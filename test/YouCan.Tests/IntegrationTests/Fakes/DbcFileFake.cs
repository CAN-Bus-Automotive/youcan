﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using YouCan.DataAccess.Core;
using YouCan.DataAccess.Models;

namespace YouCan.Tests.IntegrationTests.Fakes
{
    public class DbcFileFake : DbcFile
    {
        public DbcFileFake()
        {
            FileVersion = "0.1";

            Commands = new ObservableCollection<DbcCommand>()
            {
                new DbcCommand
                {
                    CommandName = "TestUserCommand1",
                    CommandType = CommandType.UserCommand
                },
                new DbcCommand
                {
                    CommandName = "TestUserCommand2",
                    CommandType = CommandType.UserCommand
                },
                new DbcCommand
                {
                    CommandName = "TestKeepAliveCommand1",
                    CommandType = CommandType.KeepAliveCommand
                },
                new DbcCommand
                {
                    CommandName = "TestKeepAliveCommand2",
                    CommandType = CommandType.KeepAliveCommand
                }
            };

            Messages = new List<DbcMessage>
            {
                new DbcMessage
                {
                    Signals = new List<DbcSignal>
                    {
                        new DbcSignal
                        {

                        }
                    }
                }
            };

            Vehicle = new Vehicle
            {
                Brand = "TestBrand",
                EngineType = EngineType.Diesel,
                EngineVolume = 2.0,
                Model = "TestModel",
                ModelYear = 2019
            };
        }
    }
}
