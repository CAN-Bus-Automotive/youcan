﻿using System.Threading.Tasks;
using YouCan.DataAccess.Models;
using YouCan.Services.Dbc;

namespace YouCan.Tests.IntegrationTests.Fakes
{
    public class DbcServiceFake : IDbcService
    {
        public DbcFile ReadDbcFile(string filePath)
        {
            return new DbcFileFake();
        }
    }
}
