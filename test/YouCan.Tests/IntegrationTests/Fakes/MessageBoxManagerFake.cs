﻿using System;
using System.Threading.Tasks;
using YouCan.Desctop.Infrastructure.WindowManagment.MessageBox;

namespace YouCan.Tests.IntegrationTests.Fakes
{
    public class MessageBoxManagerFake : IMessageBoxManager
    {
        public Task<string> ShowError(string title, string message)
        {
            throw new NotImplementedException();
        }

        public Task<string> ShowInfo(string title, string message)
        {
            throw new NotImplementedException();
        }
    }
}
