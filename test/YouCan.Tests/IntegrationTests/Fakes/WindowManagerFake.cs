﻿using System.Threading.Tasks;
using YouCan.Desctop.Infrastructure.WindowManagment.ApplicationWindows;

namespace YouCan.Tests.IntegrationTests.Fakes
{
    public class WindowManagerFake : IWindowManager
    {
        public Task<string> ShowFolderDialog()
        {
            return Task.FromResult("TestDirectory");
        }

        public Task<string[]> ShowOpenFileDialog()
        {
            return Task.FromResult(new string[] { "TestPathAndFileName" });
        }

        public Task<string> ShowSaveFileDialog()
        {
            return Task.FromResult("TestPath");
        }
    }
}
